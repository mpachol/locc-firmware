#include <stdint.h>
#include <unistd.h>

#include "i2c_device.hpp"
#include <iostream>
#include <iomanip> //setw
#include <fstream>
#include <ctime>
#include <cstring>
#include <cstdio>
#include <vector>
#include <string>

#define NUM_OF_LEDS   24
#define BYTES_PER_LED_EXP_1 4
#define BYTES_PER_LED_EXP_2 16


unsigned char returned_data [3096];

void print_menu()
{
    std::cout << "Enter command:" << std::endl;
    std::cout << "w = write" << std::endl;
    std::cout << "r = read" << std::endl;
    std::cout << "e = experiment 1" << std::endl;
    std::cout << "e2 = experiment 2" << std::endl;
    std::cout << "q = quit" << std::endl;
}

int main(int argc, char* argv[])
{

    //pass filename to convert binary to readable data
    if (argc == 2)
    {
        std::ifstream input_stream (argv[1], std::ifstream::in | std::ifstream::binary);
        if (input_stream)
        {
            std::string out_filename (argv[1]);
            out_filename = out_filename.replace(out_filename.end() - 3, out_filename.end(), "txt");
           
            std::ofstream output_stream(out_filename, std::ofstream::out);
          
            input_stream.seekg(0, input_stream.end);
            int length = input_stream.tellg();
            input_stream.seekg(0, input_stream.beg);

            
            if (out_filename.rfind("_e2") == std::string::npos)
            {
                //parse experiment 1
                //determine voltage resolution
                time_t raw_time = (time_t)std::stoul(out_filename.substr(out_filename.length() - 26, 10)); 
                int voltage_resolution = (length - NUM_OF_LEDS) / NUM_OF_LEDS / BYTES_PER_LED_EXP_1; 
                struct tm *timeinfo = localtime(&raw_time);
            
                if (output_stream)
                {
                    output_stream << "Test Date: " << asctime(timeinfo);
                    output_stream << "Voltage Resolution: " << voltage_resolution << std::endl;
                    output_stream << "\n\n";

                    char *packet = NULL;
                    int packet_length = voltage_resolution * BYTES_PER_LED_EXP_1 + 1;
                    for (int i = 0; i < NUM_OF_LEDS; i++)
                    {
                        packet = new char[packet_length];
                        input_stream.read(packet, packet_length);
                        output_stream.flush();
                        output_stream << "LED: " << std::dec << (int)packet[0] << "\n";
                        output_stream << "~~~~~~~~~~~~~~~~~~~~~\n";
                        for (int v = 1; v < packet_length - BYTES_PER_LED_EXP_1; v += BYTES_PER_LED_EXP_1)
                        {
                            //voltage
                            float voltage = (float)((uint8_t)packet[v] | (uint8_t)packet[v + 1] << 8) / 100.0;
                            float current = (float)((uint8_t)packet[v + 2] | (uint8_t)packet[v + 3] << 8) / 100.0;
                            output_stream << "Voltage: " << std::fixed << std::setprecision(2) << voltage << "V\n";
                            output_stream << "Current: " << std::fixed << std::setprecision(2) << current << "mA\n";
                            output_stream << "====================\n";
                        }
                        output_stream << "\n\n";
                        delete packet;
                    }
                    packet = NULL;
                }
            }
            else
            {
                //parse experiment 2
                time_t raw_time = (time_t)std::stoul(out_filename.substr(out_filename.length() - 29, 10)); 
                struct tm *timeinfo = localtime(&raw_time);

                if (output_stream)
                {
                    output_stream << "Test Date: " << asctime(timeinfo);
                    output_stream << "\n";

                    char *packet = NULL;
                    int packet_length = BYTES_PER_LED_EXP_2 + 1;
                    for (int i = 0; i < NUM_OF_LEDS; i++)
                    {
                        packet = new char[packet_length];
                        input_stream.read(packet, packet_length);
                        output_stream.flush();
                        output_stream << "LED: " << std::dec << (int)packet[0] << "\n";
                        output_stream << "~~~~~~~~~~~~~~~~~~~~~\n";
                        for (int v = 1; v <= packet_length - BYTES_PER_LED_EXP_2; v += BYTES_PER_LED_EXP_2)
                        {
                            uint16_t x = (uint16_t)((uint8_t)packet[v] | (uint8_t)packet[v + 1] << 8);
                            uint16_t y = (uint16_t)((uint8_t)packet[v + 2] | (uint8_t)packet[v + 3] << 8);
                            uint16_t z = (uint16_t)((uint8_t)packet[v + 4] | (uint8_t)packet[v + 5] << 8);
                            output_stream << "EL.X: " << x << "\n";
                            output_stream << "EL.Y: " << y << "\n";
                            output_stream << "EL.Z: " << z << "\n\n";

                            float voltage = (float)((uint8_t)packet[v + 6] | (uint8_t)packet[v + 7] << 8) / 100.0;
                            float current = (float)((uint8_t)packet[v + 8] | (uint8_t)packet[v + 9] << 8) / 100.0;
                            output_stream << "Voltage: " << std::fixed << std::setprecision(2) << voltage << "V\n";
                            output_stream << "Current: " << std::fixed << std::setprecision(2) << current << "mA\n\n";

                            x = (uint16_t)((uint8_t)packet[v + 10] | (uint8_t)packet[v + 11] << 8);
                            y = (uint16_t)((uint8_t)packet[v + 12] | (uint8_t)packet[v + 13] << 8);
                            z = (uint16_t)((uint8_t)packet[v + 14] | (uint8_t)packet[v + 15] << 8);
                            output_stream << "EL.X: " << x << "\n";
                            output_stream << "EL.Y: " << y << "\n";
                            output_stream << "EL.Z: " << z << "\n";
                            output_stream << "====================\n";
                        }
                        output_stream << "\n\n";
                        delete packet;
                    }
                    packet = NULL;
                }
                
            }

            output_stream.close();
            input_stream.close();

        }
        else
        {
            std::cerr << "Failed to open log file" << std::endl;
        }

        return 0;
    }
    else
    {
        I2CDevice csee_board(0x08);
        char user_input[256];
    
        while(true)
        {
            print_menu();
            std::cin.getline(user_input, 256);

            if (strcmp(user_input,"q") == 0)
                break;

            if (strcmp(user_input, "w") == 0)
            {
                char data[256];
                std::vector<unsigned char> string_data;
                std::cout << "Enter bytes to send separated by spaces: ";
                std::cin.getline(data, 256);
                char *pch;
                pch = strtok(data, " ");
                while (pch != NULL)
                {
                    std::string s = pch;
                    string_data.push_back(static_cast<unsigned char>(std::stoul(s, NULL, 0)));
                    pch = strtok(NULL, " ");
                }

                unsigned char *converted_data = &string_data[0];
                csee_board.i2c_write(converted_data, string_data.size());
            }
            else if (strcmp(user_input, "r") == 0)
            {
                char data[256];
                std::cout << "Enter number of bytes to receive: ";
                std::cin.getline(data, 256);
                unsigned recv_bytes = static_cast<unsigned>(std::stoul(std::string(data)));
	  	  
                csee_board.i2c_read(returned_data, recv_bytes);
                for (unsigned i = 0; i < recv_bytes; i++)
                {
                    std::cout << std::hex << (int)returned_data[i] << " ";
                }
                std::cout << std::endl;
            }
	
            else if (strcmp(user_input, "e") == 0)
            {
                time_t current_time = time(NULL);
                std::ofstream logger ("./logs/" + std::to_string(current_time) + "_csee_output.bin", std::ofstream::out | std::ofstream::binary);
	    
                uint8_t voltage_resolution [2];
                voltage_resolution[0] = 0x3;
                voltage_resolution[1] = 0x20;
                std::cout << "Setting voltage resolution to 0x" << std::hex << (int)voltage_resolution[1] << std::endl;
                csee_board.i2c_write(voltage_resolution, 2);
                sleep(1);
	    
                uint8_t start_experiment[] = {0x4};
                std::cout << "Running experiment #1" << std::endl;
                csee_board.i2c_write(start_experiment, 1);
                sleep(24);


	    
                uint8_t size_to_read[2];
                uint8_t get_size_to_read[] = {0x1};
                csee_board.i2c_write(get_size_to_read, 1);
                sleep(1);
                csee_board.i2c_read(size_to_read, 2);
                uint16_t size = (size_to_read[0] << 8) | size_to_read[1]; 
                std::cout << "CSEE firmware reports " << std::dec << size << " bytes available" << std::endl;
	    
                //uint16_t size = 1560;
	    
                uint8_t retrieve_data[] = {0x5};
                std::cout << "Preparing data from EEPROM" << std::endl;
                csee_board.i2c_write(retrieve_data, 1);
                sleep(16);
	    
                uint16_t offset = 0;
                uint16_t num_to_read = 32;
                int total_data = size;
                while (total_data > 0)
                {
                    csee_board.i2c_read((&returned_data[0] + offset), num_to_read);
                    total_data = total_data - 32;
                    if (total_data < 32 && total_data > 0)
                        num_to_read = total_data;
                    offset += 32;
                    usleep(10000);
                }
	    
                for (int i = 0; i < size; i++)
                {
                    std::cout << std::hex << std::setw(2) << std::setfill('0') << (uint16_t)returned_data[i] << " ";
                    if (i > 0 && ((i + 1) % 32 == 0))
                        std::cout << "\n";
                }
                std::cout << "\n";

                logger.write(reinterpret_cast<const char *>(returned_data), size);
                logger.close();
            }

            else if (strcmp(user_input, "e2") == 0)
            {
                time_t current_time = time(NULL);
                std::ofstream logger ("./logs/" + std::to_string(current_time) + "_csee_output_e2.bin", std::ofstream::out | std::ofstream::binary);
	    
                uint8_t voltage_resolution [2];
                voltage_resolution[0] = 0x3;
                voltage_resolution[1] = 0x20;
                std::cout << "Setting voltage resolution to 0x" << std::hex << (int)voltage_resolution[1] << std::endl;
                csee_board.i2c_write(voltage_resolution, 2);
                sleep(1);
	    
                uint8_t start_experiment[] = {0x6};
                std::cout << "Running experiment #2" << std::endl;
                csee_board.i2c_write(start_experiment, 1);
                sleep(120);


	    
                uint8_t size_to_read[2];
                uint8_t get_size_to_read[] = {0x1};
                csee_board.i2c_write(get_size_to_read, 1);
                sleep(1);
                csee_board.i2c_read(size_to_read, 2);
                uint16_t size = (size_to_read[0] << 8) | size_to_read[1]; 
                std::cout << "CSEE firmware reports " << std::dec << size << " bytes available" << std::endl;
	    
                //uint16_t size = 1560;
	    
                uint8_t retrieve_data[] = {0x5};
                std::cout << "Preparing data from EEPROM" << std::endl;
                csee_board.i2c_write(retrieve_data, 1);
                sleep(16);
	    
                uint16_t offset = 0;
                uint16_t num_to_read = 32;
                int total_data = size;
                while (total_data > 0)
                {
                    csee_board.i2c_read((&returned_data[0] + offset), num_to_read);
                    total_data = total_data - 32;
                    if (total_data < 32 && total_data > 0)
                        num_to_read = total_data;
                    offset += 32;
                    usleep(10000);
                }
	    
                for (int i = 0; i < size; i++)
                {
                    std::cout << std::hex << std::setw(2) << std::setfill('0') << (uint16_t)returned_data[i] << " ";
                    if (i > 0 && ((i + 1) % 32 == 0))
                        std::cout << "\n";
                }
                std::cout << "\n";

                logger.write(reinterpret_cast<const char *>(returned_data), size);
                logger.close();
            }
        }

        return 0;   
    }
}
