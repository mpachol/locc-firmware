#include <fcntl.h>
#include <iostream>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "i2c_device.hpp"

I2CDevice::I2CDevice(int da) : device_address(da)
{

  if ((device_descriptor = open("/dev/i2c-1", O_RDWR)) < 0)
    {
      //throw
      std::cerr << "could not open device" << std::endl;
    }


  if (ioctl(device_descriptor, I2C_SLAVE, da) < 0)
    {
      //throw
      std::cerr << "problem with ioctl" << std::endl;
    }
}

void I2CDevice::i2c_read(void *data, int num_bytes)
{
  if (read(device_descriptor, data, num_bytes) < 0)
    {
      //throw
    }
}

void I2CDevice::i2c_write(void *data, int num_bytes)
{
  
  if (write(device_descriptor, data, num_bytes) < 0)
    {
      //throw
    }
}

