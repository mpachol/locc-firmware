#ifndef _I2C_DEVICE_HPP_
#define _I2C_DEVICE_HPP_


class I2CDevice
{

public:

  I2CDevice(int da);
  void i2c_read(void *data, int num_bytes);
  void i2c_write(void *data, int size);
  int device_address;
  int device_descriptor;

};

#endif
