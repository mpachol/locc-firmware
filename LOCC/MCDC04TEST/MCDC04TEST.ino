#include <MCDC04.h>

#include <stdlib.h>
#include <Wire.h>
#include <SPI.h>
//#include <BitBangI2C.h>

#define MCDC04_ADDRESS_1 (0b11101000)
#define MCDC04_ADDRESS_2 (0b11101010)
#define MCDC04_ADDRESS_3 (0b11101100)

#define pin (40)
MCDC04 MCDC04_1 = MCDC04(MCDC04_ADDRESS_1, pin);

//float sensorVals[] = {0,0,0,0} ;

void setup() 
{
  Serial.begin(9600);
  MCDC04_1.begin();
  
  //Serial.println("CLEARDATA");  //clears up any data left from previous projects
  
  SPI.setBitOrder(MSBFIRST); //Send MSB first  
 
}

void loop() 
{
  
  uint16_t TrD , X, Z, Y;
  MCDC04_SetConfig();
  
  delay(500);


  MCDC04_1.GetData(&TrD, &X, &Z, &Y);

  //Print over the serial line to send to Processing
  //Printing in order X Y Z ; in Decimal
  
  //Serial.print(TrD, HEX); Serial.print(",");
  Serial.print(X, HEX);Serial.print(",");
  Serial.print(Y, HEX); Serial.print(",");
  Serial.println(Z, HEX);// Serial.print(",");

  delay(500);

}
void MCDC04_SetConfig()
{
  MCDC04_1.SetCREGL(0x88);  //Anodes Set and Input, Reference Current =  20 nA, Integration Time = 256 ms
  MCDC04_1.SetCREGH(0x09); 
  MCDC04_1.SetOPT(0x00);
  MCDC04_1.SetEdges(0x01) ;
  MCDC04_1.SetBreak(0x20) ;
  
  
}


