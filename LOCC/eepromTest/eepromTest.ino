

#include <Wire.h>


#include "EEPROMex.h"
#include "EEPROMVar.h"

#define DATA_SIZE_ADDRESS (E2END - 1)
#define VOLTAGE_RESOLUTION_ADDRESS (E2END - 2)

#define ALLOCATED_REGISTERS (11)

uint16_t eepromAddressCounter = 0;


void setup() 
{
  
  Serial.begin(9600);
  Wire.begin();
  
}

char serialCharacter = 0;
String serialCommandInput = "";

void loop() 
{
  // put your main code here, to run repeatedly:
  if(Serial.available() > 0)
  {
    serialCharacter = Serial.read();

    if(serialCharacter != '\n')
    {
      serialCommandInput += serialCharacter;
    }
    else
    {
      Serial.println("Input Received");
      ProcessCommand(serialCommandInput);
      serialCommandInput = "";
    }
  }
 
}

void configureToWriteData(int maxAllowedWrites)
{
  EEPROM.setMemPool(0, E2END);
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
}

void IncrementCounter(int bytesToIncrement)
{
  for(int i = 0; i < bytesToIncrement; i++)
  {
    eepromAddressCounter++;
  }
}

void UpdateDataSize()
{
  ConfigureToWriteToRegister(2);
  EEPROM.updateInt(DATA_SIZE_ADDRESS, eepromAddressCounter);
}

void ProcessCommand(String serialCommandInput)
{
  if(serialCommandInput.equals("DATAWRITE"))
  {
    Serial.println("Writing data to memory space");
    DATAWRITE();
  }
  else if(serialCommandInput.equals("DATASIZE"))
  {
    Serial.print(DATASIZE());
    Serial.println(" Bytes Available");
  }
}

void DATAWRITE()
{
    delay(5);
 
    ConfigureToWriteData(2);
    EEPROM.updateInt(eepromAddressCounter, 0x01);
    IncrementCounter(2);
    UpdateDataSize();
 
}

uint16_t DATASIZE()
{
  return EEPROM.readInt(DATA_SIZE_ADDRESS);
}

void ConfigureToWriteToRegister(int maxAllowedWrites)
{
  EEPROM.setMemPool(E2END - ALLOCATED_REGISTERS, E2END + 1);
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
}

void ConfigureToWriteData(int maxAllowedWrites)
{
  EEPROM.setMemPool(0, E2END);
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
}

void RetrieveData(uint8_t* dataArray)
{
  ConfigureToWriteData(0);
  for(int i = 0; i < eepromAddressCounter; i++)
  {
    dataArray[i] = EEPROM.readByte(i);
  }
}
