#include "Arduino.h"
#include "MCDC04.h"
#include <Wire.h>
#include<BitBangI2C.h>

void MCDC04::begin(void)
{
	i2c = BitBangI2C(); 
}

MCDC04::MCDC04(uint8_t _address, uint8_t pin)
{
	i2c_address = _address;
	_pin = pin;
}

void MCDC04::writeToRegister(uint8_t address, uint8_t data)
{
	i2c.beginTransmission(i2c_address); //Begin Transmission
	i2c.write(MCDC04_OSR);
    i2c.write(0x42); //Configuration Mode Register pointer
	i2c.i2c_restart(i2c_address);
    i2c.write(address);    
	i2c.write(data); //Data to write
    i2c.endTransmission();
}

void MCDC04::readFromRegister(uint8_t *CREGL, uint8_t *CREGH, uint8_t *OPTREG, uint8_t *BREAK, uint8_t *EDGES)
{	//(uint8_t *CREGL, uint8_t *CREGH, uint8_t *OPTREG, uint8_t *BREAK, uint8_t *EDGES)
	uint8_t received_data = 0;

	// Establish connection, select receipt address
	i2c.beginTransmission(i2c_address);
	i2c.write(MCDC04_CREGL);
	//i2c.endTransmission();

	// Request one data byte
	i2c.requestFrom(i2c_address);
    *CREGL = i2c.read(false);
	*CREGH = i2c.read(false);
	*OPTREG = i2c.read(false);
	*BREAK = i2c.read(false);
	*EDGES = i2c.read(true);
	
	i2c.endTransmission();
	//return received_data;
}

void MCDC04::SetCREGL(uint8_t _L) //CREGL
{
	writeToRegister(MCDC04_CREGL, _L);
}
/*
uint8_t MCDC04::GetCREGL(void) //CREGL
{
	return readFromRegister(MCDC04_CREGL);
}
*/
void MCDC04::SetCREGH(uint8_t _H) //CREGH
{
	writeToRegister(MCDC04_CREGH, _H);
}
/*
uint8_t  MCDC04::GetCREGH(void) //CREGH
{
	return readFromRegister(MCDC04_CREGH);
}
	*/
void MCDC04::SetState(uint8_t _M) //Operational State
{
	writeToRegister(MCDC04_OSR, _M);
}
/*
uint8_t MCDC04::GetState(void) //Operational State
{
	return readFromRegister(MCDC04_OSR);
}
	*/
void MCDC04::SetOPT(uint8_t _O) //Options Registers
{
	writeToRegister(MCDC04_OPTREG, _O);
}
/*
uint8_t MCDC04::GetOPT(void)
{
	return readFromRegister(MCDC04_OPTREG);
}
*/

void MCDC04::SetEdges(uint8_t _E) //Edges Register
{
	writeToRegister(MDCD04_EDGES, _E);
}

void MCDC04::SetBreak(uint8_t _B) //Edges Register
{
	writeToRegister(MDCD04_BREAK, _B);
}

void MCDC04::clearRegisters(void)
{
	writeToRegister(MCDC04_CREGL, 0x00);
	writeToRegister(MCDC04_CREGH, 0x00);
	writeToRegister(MCDC04_OSR, 0x00);
	writeToRegister(MCDC04_OPTREG, 0x00);
	writeToRegister(MCDC04_OUT0, 0x00);
	writeToRegister(MCDC04_OUT1, 0x00);
	writeToRegister(MCDC04_OUT2, 0x00);
	writeToRegister(MCDC04_OUT3, 0x00);
}

void MCDC04::GetData(uint16_t *X, uint16_t *Y, uint16_t *Z)
{
	i2c.beginTransmission(i2c_address); //Begin Transmission
	i2c.write(MCDC04_OSR);
    i2c.write(0b11000011); //Measurement Mode w/ PD pointer
	i2c.endTransmission();
	
	/*wait for ready pin
	 for( uint16_t i = 0x8000; i > 0x00; i--){
		 int val = digitalRead( _pin) ;
		 if( val == HIGH){
		 break ;
		 }
	 }
*/
	delay(2000);
	i2c.beginTransmission(i2c_address);
	i2c.write(MCDC04_OUT0);  //send address pointer
	i2c.requestFrom(i2c_address);
	
	//read X color
	//i2c.requestFrom(i2c_address);
	//i2c.write(MCDC04_OUT0);  //send address pointer
	uint16_t tmpX = (uint16_t)i2c.read(false);
	tmpX |=  (uint16_t)i2c.read(false)   << 8;
	
	//read Y color
	//i2c.requestFrom(i2c_address);
	uint16_t tmpY = (uint16_t)i2c.read(false);
	tmpY |= (uint16_t) i2c.read(false)  << 8;
	
	//read Z color
	//i2c.requestFrom(i2c_address);
	uint16_t tmpZ = i2c.read(false) ;
	tmpZ |=  (uint16_t) i2c.read(true)  <<8;
	
	//power down
	i2c.write(MCDC04_OSR);
    i2c.write(0b01000010); //Measurement Mode Register pointer
	
	//data fetch
	//if(*X != NULL){
	*X = tmpX ;
	
	
	//if(*Y != NULL){
	*Y = tmpY ;
	
	
	//if(*Z != NULL){
	*Z = tmpZ ;
	
	
	i2c.endTransmission();
}

uint16_t MCDC04::GetX(void)
{
	i2c.beginTransmission(i2c_address); //Begin Transmission
	i2c.write(MCDC04_OSR);
    i2c.write(0x83); //Measurement Mode Register pointer
	i2c.endTransmission();
	
	//wait for ready pin
	 for( uint16_t i = 0x8000; i > 0x00; i--){
		 int val = digitalRead( _pin) ;
		 if( val == HIGH){
		 break ;
		 }
	 }

	i2c.beginTransmission(i2c_address);
	i2c.write(MCDC04_OUT0);  //send address pointer
	i2c.i2c_restart(i2c_address);
	
	//read X color
	i2c.requestFrom(i2c_address);
	uint16_t tmpX = (uint16_t)i2c.read(false);
	tmpX |=  (uint16_t)i2c.read(false)   << 8;
	uint16_t X = tmpX;
	return X ;
	i2c.endTransmission();
}

uint16_t MCDC04::GetY(void)
{
	//i2c.beginTransmission(i2c_address); //Begin Transmission
	i2c.write(MCDC04_OSR);
    i2c.write(0x83); //Measurement Mode Register pointer
	i2c.endTransmission();
	
	//wait for ready pin
	 for( uint16_t i = 0x8555; i > 0x00; i--){
		int val = digitalRead( _pin) ;
		if( val == HIGH){
		break ;
		}
	 }

	i2c.beginTransmission(i2c_address);
	i2c.write(MCDC04_OUT1);  //send address pointer
	i2c.i2c_restart(i2c_address);
	
	//read Y color
	i2c.requestFrom(i2c_address);
	uint16_t tmpY = (uint16_t)i2c.read(false);
	tmpY |=  (uint16_t)i2c.read(false)   << 8;
	uint16_t Y = tmpY;
	return Y ;
	i2c.endTransmission();
}

uint16_t MCDC04::GetZ(void)
{
	//i2c.beginTransmission(i2c_address); //Begin Transmission
	i2c.write(MCDC04_OSR);
    i2c.write(0x83); //Measurement Mode Register pointer
	i2c.endTransmission();
	
	//wait for ready pin
	 for( uint16_t i = 0x8000; i > 0x00; i--){
		int val = digitalRead( _pin) ;
		if( val == HIGH){
		break ;
		}
	 }

	i2c.beginTransmission(i2c_address);
	i2c.write(MCDC04_OUT2);  //send address pointer
	i2c.i2c_restart(i2c_address);
	
	//read X color
	i2c.requestFrom(i2c_address);
	uint16_t tmpZ = (uint16_t)i2c.read(false);
	tmpZ |=  (uint16_t)i2c.read(false)   << 8;
	uint16_t Z = tmpZ;
	return Z ;
	i2c.endTransmission();
}