/*
Copyright (C) 2011 James Coliz, Jr. <maniacbug@ymail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.
*/

#include "Arduino.h"
#include "MCP23018_BitBang.h"

void MCP23018_BitBang::writeToRegister(uint8_t address, uint8_t data)
{
	i2c.beginTransmission(i2c_address);
    i2c.write(address);
    i2c.write(data);       
    i2c.endTransmission();
}

uint8_t MCP23018_BitBang::readFromRegister(uint8_t address)
{
	uint8_t received_data = 0;

	// Establish connection, select receipt address
	i2c.beginTransmission(i2c_address);
	i2c.write(address);
	i2c.endTransmission();

	// Request one data byte
	i2c.requestFrom(i2c_address);

    received_data = i2c.read(true);

	i2c.endTransmission();

	return received_data;
}

MCP23018_BitBang::MCP23018_BitBang(uint8_t _address)
{
	i2c_address = _address;
}

void MCP23018_BitBang::begin(void)
{
	i2c = BitBangI2C();
}

void MCP23018_BitBang::SetIODIRA(uint8_t _a)
{
	writeToRegister(IODIRA, _a);
}

void MCP23018_BitBang::SetIODIRB(uint8_t _b)
{
	writeToRegister(IODIRB, _b);
}

uint8_t MCP23018_BitBang::GetIODIRA(void)
{
	return readFromRegister(IODIRA);
}

uint8_t MCP23018_BitBang::GetIODIRB(void)
{
	return readFromRegister(IODIRB);
}

void MCP23018_BitBang::SetPullupsA(uint8_t _a)
{
	writeToRegister(GPPUA, _a);
}

void MCP23018_BitBang::SetPullupsB(uint8_t _b)
{
	writeToRegister(GPPUB, _b);
}

uint8_t MCP23018_BitBang::GetPullupsA(void)
{
	return readFromRegister(GPPUA);
}

uint8_t MCP23018_BitBang::GetPullupsB(void)
{
	return readFromRegister(GPPUB);
}

void MCP23018_BitBang::SetPortA(uint8_t _data)
{
	writeToRegister(GPIOA, _data);
}

void MCP23018_BitBang::SetPortB(uint8_t _data)
{
	writeToRegister(GPIOB, _data);
}

uint8_t MCP23018_BitBang::GetPortA(void)
{
	return readFromRegister(GPIOA);
}

uint8_t MCP23018_BitBang::GetPortB(void)
{
	return readFromRegister(GPIOB);
}

void MCP23018_BitBang::SetLatchPortA(uint8_t dataByte)
{
	writeToRegister(OLATA, dataByte);
}

void MCP23018_BitBang::SetLatchPortB(uint8_t dataByte)
{
	writeToRegister(OLATB, dataByte);
}

uint8_t MCP23018_BitBang::GetLatchPortA(void)
{
	return readFromRegister(OLATA);
}

uint8_t MCP23018_BitBang::GetLatchPortB(void)
{
	return readFromRegister(OLATB);
}