#include "BitBangI2C.h"

BitBangI2C::BitBangI2C()
{
	i2c_init();
}
	
uint8_t BitBangI2C::beginTransmission(uint8_t address)
{
	return	i2c_start(address | I2C_WRITE);
}
	
uint8_t BitBangI2C::requestFrom(uint8_t address)
{
	uint8_t firstACK = i2c_rep_start(address | I2C_READ);
	return firstACK;
}

uint8_t BitBangI2C::i2c_restart(uint8_t address)
{
	uint8_t firstACK = i2c_rep_start(address | I2C_WRITE);
	return firstACK;
}

void BitBangI2C::endTransmission()
{
	i2c_stop();
}

uint8_t BitBangI2C::write(uint8_t dataByte)
{
	return i2c_write(dataByte);
}
	
uint8_t BitBangI2C::read(bool lastByte)
{
	return i2c_read(lastByte);
}