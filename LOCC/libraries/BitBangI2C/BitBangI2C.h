#ifndef BITBANGI2C_H
#define BITBANGI2C_H

#define I2C_FASTMODE 1

#include "Arduino.h"

#include "SoftI2CMaster.h"

class BitBangI2C{

public:
	BitBangI2C();
	uint8_t beginTransmission(uint8_t address);
	uint8_t requestFrom(uint8_t address);
	uint8_t i2c_restart(uint8_t address);

	void endTransmission();

	uint8_t write(uint8_t dataByte);
	uint8_t read(bool lastByte);
};

#endif