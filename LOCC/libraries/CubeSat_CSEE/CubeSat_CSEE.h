#ifndef CUBESAT_CSEE_H
#define CUBESAT_CSEE_H

#include <stdlib.h>

#include "Arduino.h"

#include "BitBangI2C.h"
#include "MCP23018_BitBang.h"
#include "Adafruit_INA219_BitBang.h"
#include <SPI.h>
#include "EEPROMex.h"
#include "EEPROMVar.h"
#include "MCDC04.h"

#define DATA_SIZE_ADDRESS (E2END - 1)
#define VOLTAGE_RESOLUTION_ADDRESS (E2END - 2)
#define CURRENT_LED_ADDRESS (E2END - 3)
#define COMPONENT_STATUS_ADDRESS (E2END - 4)

#define LED_STATUS_ARRAY3_ADDRESS (E2END - 5)
#define LED_STATUS_ARRAY2_ADDRESS (E2END - 6)
#define LED_STATUS_ARRAY1_ADDRESS (E2END - 7)

#define LED_ENABLED_ARRAY3_ADDRESS (E2END - 8)
#define LED_ENABLED_ARRAY2_ADDRESS (E2END - 9)
#define LED_ENABLED_ARRAY1_ADDRESS (E2END - 10)

#define ALLOCATED_REGISTERS (11)

#define SIZEOF_CURRENT (2)
#define SIZEOF_VOLTAGE (2)
#define SIZEOF_LED_MARKER (1)
#define SIZEOF_TIME_STAMP (2)

#define GPIO_ADDRESS_1 (0x40)
#define GPIO_1_STATUS_BIT (6)
#define GPIO_ADDRESS_2 (0x4E)
#define GPIO_2_STATUS_BIT (7)
#define INA219_ADDRESS (0x80)
#define INA219_STATUS_BIT (1)
#define DAC_PIN (10)
#define DAC_STATUS_BIT (0)
#define MCDC04_ADDRESS_1 (0b11101000) //E8
#define MCDC04_ADDRESS_2 (0b11101010) //EA
#define MCDC04_ADDRESS_3 (0b11101100) //EC

#define VOLTAGE_READ_PIN A0

class CubeSat_CSEE
{
public:
	CubeSat_CSEE();

	void Begin();

	uint16_t GetDataSize();

	uint8_t GetVoltageResolution();
	void SetVoltageResolution(byte voltageResolutionInput);

	uint8_t GetCurrentLED();

	uint8_t GetComponentStatus();
	void GetLEDStatus(uint8_t* ledStatusArray);
	void CheckComponentStatus();
	void CheckLEDStatus();

	void GetEnabledLEDs(uint8_t* ledEnabledArray);
	void EnableLED(int LEDToEnable);
	void DisableLED(int LEDToDisable);

	void EnableArray(int arrayToEnable);
	void DisableArray(int arrayToDisable);

	uint16_t RunExperiment1();
	uint16_t RunExperiment2();

	void RetrieveData(uint8_t dataArray[32], uint16_t packetID);
	void TestEEPROM();

	void TestRAM();
	void RetrieveSavedData(uint8_t* dataArray, uint16_t packetID);
	uint8_t savedData[2048];

private:
	Adafruit_INA219_BitBang ina219 = Adafruit_INA219_BitBang(INA219_ADDRESS);
	MCP23018_BitBang mcp23018_1 = MCP23018_BitBang(GPIO_ADDRESS_1);
	MCP23018_BitBang mcp23018_2 = MCP23018_BitBang(GPIO_ADDRESS_2);

	uint16_t eepromAddressCounter;

	uint8_t CheckMCP23018Status(MCP23018_BitBang mcp23018Input);
	uint8_t CheckMCP4901Status();
	uint8_t CheckINA219Status(Adafruit_INA219_BitBang ina219Input);

	void ConfigureComponents();
	void ConfigureMCP23018(MCP23018_BitBang mcp23018);
	void ConfigureMCP4901();
	void ConfigureINA219(Adafruit_INA219_BitBang ina219Input);
	
	void ConfigureToWriteToRegister(int maxAllowedWrites);
	void ConfigureToWriteData(int maxAllowedWrites);
	
	void dacTransfer(int slaveNumber, int value);

	void DisableLED(uint8_t LEDByteMask, int addressToMask);
	void EnableLED(uint8_t LEDByteMask, int addressToMask);

	uint8_t GetByteMask(int LEDToMask);

	uint16_t GetMaskAddress(int LEDToMask);

	uint16_t GetSizeOfIndividualLEDData();

	bool IsEEPROMFull();

	void IncrementCurrentLED(uint8_t currentLEDInput);
	void UpdateDataSize();

	void VoltageRamp();
	void IncrementCounter(int bytesToIncrement);
	void DecrementCounter(int bytesToDecrement);

	void WriteLEDInfo();

	void MCDC04_SetConfig();
	void AquireEL(MCDC04 tmp);
	void VoltageSet();

	uint16_t GetTimeStamp();
};
#endif