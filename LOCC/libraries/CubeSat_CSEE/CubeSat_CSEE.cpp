#include "CubeSat_CSEE.h"


MCDC04 MCDC04_1 = MCDC04(MCDC04_ADDRESS_1, 40);
MCDC04 MCDC04_2 = MCDC04(MCDC04_ADDRESS_2, 40);
MCDC04 MCDC04_3 = MCDC04(MCDC04_ADDRESS_3, 40);


// Public Functions
CubeSat_CSEE::CubeSat_CSEE()
{

}

void CubeSat_CSEE::Begin()
{

	ConfigureComponents();
	//CheckComponentStatus();
	//CheckLEDStatus();
	
}

uint16_t GetDataSize()
{
	return EEPROM.readInt(DATA_SIZE_ADDRESS);
}

uint8_t CubeSat_CSEE::GetVoltageResolution()
{
	return EEPROM.readByte(VOLTAGE_RESOLUTION_ADDRESS);
}

void CubeSat_CSEE::SetVoltageResolution(uint8_t voltageResolutionInput)
{
	ConfigureToWriteToRegister(1);
	EEPROM.updateByte(VOLTAGE_RESOLUTION_ADDRESS, voltageResolutionInput);
}

uint8_t CubeSat_CSEE::GetCurrentLED()
{
	return EEPROM.readByte(CURRENT_LED_ADDRESS);
}

/*uint8_t CubeSat_CSEE::GetComponentStatus()
{
	return EEPROM.readByte(COMPONENT_STATUS_ADDRESS);
}

void CubeSat_CSEE::GetLEDStatus(uint8_t* ledStatusArray)
{
	ledStatusArray[0] = EEPROM.readByte(LED_STATUS_ARRAY1_ADDRESS);
	ledStatusArray[1] = EEPROM.readByte(LED_STATUS_ARRAY2_ADDRESS);
	ledStatusArray[2] = EEPROM.readByte(LED_STATUS_ARRAY3_ADDRESS);
}

void CubeSat_CSEE::CheckComponentStatus()
{
	uint8_t componentStatusByte = 0;

	componentStatusByte |= (CheckMCP23018Status(mcp23018_1) << GPIO_1_STATUS_BIT);
	componentStatusByte |= (CheckMCP23018Status(mcp23018_2) << GPIO_2_STATUS_BIT);
	componentStatusByte |= (CheckINA219Status(ina219) << INA219_STATUS_BIT);
	componentStatusByte |= (CheckMCP4901Status() << DAC_STATUS_BIT);

	ConfigureToWriteToRegister(1);
	EEPROM.updateByte(COMPONENT_STATUS_ADDRESS, componentStatusByte);
}

void CubeSat_CSEE::CheckLEDStatus()
{
	uint8_t array1Status = 0;
	uint8_t array2Status = 0;
	uint8_t array3Status = 0;

	if(CheckMCP4901Status())
	{
		dacTransfer(DAC_PIN, 3072);
		float current;

		if(CheckMCP23018Status(mcp23018_1))
		{
			for(int i = 0; i < 8; i++)
			{
				current = 0;
				mcp23018_1.SetPortA(1 << i);
				current = ina219.getCurrent_mA();
				if(current > 5)
				{
					array1Status |= (1 << i);
				}

				mcp23018_1.SetPortA(0x00);
			}

			for(int i = 0; i < 8; i++)
			{
				current = 0;
				mcp23018_1.SetPortB(1 << i);
				current = ina219.getCurrent_mA();
				if(current > 1)
				{
					array2Status |= (1 << i);
				}

				mcp23018_1.SetPortB(0x00);
			}
		}

		if(CheckMCP23018Status(mcp23018_2))
		{
			for(int i = 0; i < 8; i++)
			{
				mcp23018_2.SetPortA(1 << i);
				if(ina219.getCurrent_mA() > 5)
				{
					array3Status |= (1 << i);
				}

				mcp23018_2.SetPortA(0x00);
			}
		}
	}

	ConfigureToWriteToRegister(3);
	EEPROM.updateByte(LED_STATUS_ARRAY1_ADDRESS, array1Status);
	EEPROM.updateByte(LED_STATUS_ARRAY2_ADDRESS, array2Status);
	EEPROM.updateByte(LED_STATUS_ARRAY3_ADDRESS, array3Status);
}

void CubeSat_CSEE::GetEnabledLEDs(uint8_t* ledEnabledArray)
{
	ledEnabledArray[0] = EEPROM.readByte(LED_ENABLED_ARRAY1_ADDRESS);
	ledEnabledArray[1] = EEPROM.readByte(LED_ENABLED_ARRAY2_ADDRESS);
	ledEnabledArray[2] = EEPROM.readByte(LED_ENABLED_ARRAY3_ADDRESS);
}

void CubeSat_CSEE::EnableLED(int LEDToEnable)
{
	EnableLED(GetByteMask(LEDToEnable), GetMaskAddress(LEDToEnable));
}

void CubeSat_CSEE::DisableLED(int LEDToDisable)
{
	DisableLED(GetByteMask(LEDToDisable), GetMaskAddress(LEDToDisable));
}

void CubeSat_CSEE::EnableArray(int arrayToEnable)
{
	if(arrayToEnable == 1)
	{
		EnableLED(0b11111111, LED_ENABLED_ARRAY1_ADDRESS);
	}
	else if(arrayToEnable == 2)
	{
		EnableLED(0b11111111, LED_ENABLED_ARRAY2_ADDRESS);
	}
	else
	{
		EnableLED(0b11111111, LED_ENABLED_ARRAY3_ADDRESS);
	}
}

void CubeSat_CSEE::DisableArray(int arrayToDisable)
{
	if(arrayToDisable == 1)
	{
		EnableLED(0b00000000, LED_ENABLED_ARRAY1_ADDRESS);
	}
	else if(arrayToDisable == 2)
	{
		EnableLED(0b00000000, LED_ENABLED_ARRAY2_ADDRESS);
	}
	else
	{
		EnableLED(0b00000000, LED_ENABLED_ARRAY3_ADDRESS);
	}
}*/

uint16_t CubeSat_CSEE::RunExperiment1()
{
	

	eepromAddressCounter = 0;
	int ledCounter = 0;

	while(!IsEEPROMFull() && ledCounter < 8)
	{
		WriteLEDInfo();
		mcp23018_1.SetPortA(1 << ledCounter);
		VoltageRamp();
		mcp23018_1.SetPortA(0x00);

		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	while(!IsEEPROMFull() && ledCounter < 12)
	{
		
		WriteLEDInfo();
		mcp23018_1.SetPortB(1 << ledCounter - 8);
		VoltageRamp();
		mcp23018_1.SetPortB(0x00);

		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	while(!IsEEPROMFull() && ledCounter < 20)
	{
		
		WriteLEDInfo();
		mcp23018_2.SetPortA(1 << ledCounter - 12);
		VoltageRamp();
		mcp23018_2.SetPortA(0x00);
		
		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	while(!IsEEPROMFull() && ledCounter < 24)
	{
		
		WriteLEDInfo();
		mcp23018_1.SetPortB(1 << ledCounter - 20);
		VoltageRamp();
		mcp23018_1.SetPortB(0x00);

		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	return eepromAddressCounter;
}

uint16_t CubeSat_CSEE::RunExperiment2()
{
	//uint8_t ledsToRunArray1 = EEPROM.readByte(LED_ENABLED_ARRAY1_ADDRESS) & EEPROM.readByte(LED_STATUS_ARRAY1_ADDRESS);
	//uint8_t ledsToRunArray2 = EEPROM.readByte(LED_ENABLED_ARRAY2_ADDRESS) & EEPROM.readByte(LED_STATUS_ARRAY2_ADDRESS);
	//uint8_t ledsToRunArray3 = EEPROM.readByte(LED_ENABLED_ARRAY3_ADDRESS) & EEPROM.readByte(LED_STATUS_ARRAY3_ADDRESS);


	eepromAddressCounter = 0;
	int ledCounter = 0;
	while(!IsEEPROMFull() && ledCounter < 8)
	{

		WriteLEDInfo();
		mcp23018_1.SetPortA(1 << ledCounter);
		MCDC04_SetConfig();
		AquireEL(MCDC04_1);
		VoltageSet();
		MCDC04_SetConfig();
		AquireEL(MCDC04_1);
		mcp23018_1.SetPortA(0x00);
		dacTransfer(DAC_PIN, 0);

		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	while(!IsEEPROMFull() && ledCounter < 16)
	{
		
		WriteLEDInfo();
		mcp23018_1.SetPortB(1 << ledCounter - 8);
		MCDC04_SetConfig();
		AquireEL(MCDC04_2);
		VoltageSet();
		MCDC04_SetConfig();
		AquireEL(MCDC04_2);
		mcp23018_1.SetPortB(0x00);
		dacTransfer(DAC_PIN, 0);
		
		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	while(!IsEEPROMFull() && ledCounter < 24)
	{
		
		WriteLEDInfo();
		mcp23018_2.SetPortA(1 << ledCounter - 16);
		MCDC04_SetConfig();
		AquireEL(MCDC04_3);
		VoltageSet();
		MCDC04_SetConfig();
		AquireEL(MCDC04_3);
		mcp23018_2.SetPortA(0x00);
		dacTransfer(DAC_PIN, 0);
		

		UpdateDataSize();
		ledCounter++;
		IncrementCurrentLED(ledCounter);
	}

	return eepromAddressCounter;
}

void CubeSat_CSEE::RetrieveData(uint8_t dataArray[32], uint16_t packetID)
{
	ConfigureToWriteData(0);
	for(int i = packetID * 32; i < ((packetID * 32) + 32); i++)
	{
		dataArray[i - (packetID * 32)] = EEPROM.readByte(i);
	}
}
// Private Functions

/*uint8_t CubeSat_CSEE::CheckMCP23018Status(MCP23018_BitBang mcp23018Input)
{
	uint8_t mcp23018Status = 1;

	if(!(mcp23018Input.GetIODIRA() == 0x00))
	{
		mcp23018Status = 0;
	}
	else if(!(mcp23018Input.GetIODIRB() == 0x00))
	{
		mcp23018Status = 0;
	}
	else if(!(mcp23018Input.GetPullupsA() == 0xFF))
	{
		mcp23018Status = 0;
	}
	else if(!(mcp23018Input.GetPullupsB() == 0xFF))
	{
		mcp23018Status = 0;
	}
	else if(!(mcp23018Input.GetPortA() == 0x00))
	{
		mcp23018Status = 0;
	}
	else if(!(mcp23018Input.GetPortB() == 0x00))
	{
		mcp23018Status = 0;
	}
	return mcp23018Status;
}

uint8_t CubeSat_CSEE::CheckMCP4901Status()
{
	uint8_t mcp4901Status = 1;

	dacTransfer(DAC_PIN, 0);
	delay(1);
	if(!(analogRead(VOLTAGE_READ_PIN) == 0))
	{
		mcp4901Status = 0;
	}

	dacTransfer(DAC_PIN, 2048);
	delay(1);
	if(!(abs(analogRead(VOLTAGE_READ_PIN) - 612) > 10))
	{
		mcp4901Status = 0;
	}

	dacTransfer(DAC_PIN, 1024);
	delay(1);
	if(!(abs(analogRead(VOLTAGE_READ_PIN) - 1024) > 10))
	{
		mcp4901Status = 0;
	}

	return mcp4901Status;
}

uint8_t CubeSat_CSEE::CheckINA219Status(Adafruit_INA219_BitBang ina219Input)
{
	uint8_t ina219Status = 1;

	if(!(ina219Input.GetCalibrationRegister() == 8192))
	{
		ina219Status = 0;
	}

	return ina219Status;
}*/

void CubeSat_CSEE::ConfigureComponents()
{
	ConfigureMCP23018(mcp23018_1);
	ConfigureMCP23018(mcp23018_2);

	ConfigureINA219(ina219);

	ConfigureMCP4901();
}

void CubeSat_CSEE::ConfigureMCP23018(MCP23018_BitBang mcp23018Input)
{
	mcp23018Input.begin();

	mcp23018Input.SetIODIRA(0x00);
	mcp23018Input.SetIODIRB(0x00);

	mcp23018Input.SetPullupsA(0xFF);
	mcp23018Input.SetPullupsB(0xFF);

	mcp23018Input.SetLatchPortA(0xFF);
	mcp23018Input.SetLatchPortB(0xFF);

	mcp23018Input.SetPortA(0x00);
	mcp23018Input.SetPortB(0x00);
}

void CubeSat_CSEE::ConfigureMCP4901()
{
	SPI.begin();
	SPI.setBitOrder(MSBFIRST); //Send MSB first
	pinMode(DAC_PIN, OUTPUT);
	pinMode(VOLTAGE_READ_PIN, INPUT);
}

void CubeSat_CSEE::ConfigureINA219(Adafruit_INA219_BitBang ina219Input)
{
	ina219.begin();
	ina219Input.setCalibration_16V_400mA();
}

void CubeSat_CSEE::ConfigureToWriteToRegister(int maxAllowedWrites)
{
	EEPROM.setMemPool(E2END - ALLOCATED_REGISTERS, E2END + 1);
	EEPROM.setMaxAllowedWrites(maxAllowedWrites);
}

void CubeSat_CSEE::ConfigureToWriteData(int maxAllowedWrites)
{
	EEPROM.setMemPool(0, E2END);
	EEPROM.setMaxAllowedWrites(maxAllowedWrites);
}

void CubeSat_CSEE::dacTransfer(int slaveNumber, int value)
{
	// 16-bit value is split into two bytes
	// 4 most significant bits correspond to configuration of the DAC
	// 12 least significant bits correspond to 12-bit data
	byte upperData = 0x30;
	// Set the 4 configuration bits
	byte lowerData = 0x00;
	word dataWord;
	// The 16-bit word to be written
	dataWord = value;
	lowerData = lowByte(dataWord);

	//transfer the 4 most significant bits of data to the upper byte
	upperData |= highByte(dataWord);

	digitalWrite(slaveNumber, LOW);
	//Transfer the data
	SPI.transfer(upperData);
	SPI.transfer(lowerData);
	//DAC is unselected
	digitalWrite(slaveNumber, HIGH);
}

/*void CubeSat_CSEE::DisableLED(uint8_t LEDToDisable, int arrayAddressToMask)
{
	uint8_t currentlyEnabledLEDs = EEPROM.readByte(arrayAddressToMask);

	ConfigureToWriteToRegister(1);
	EEPROM.updateByte(arrayAddressToMask, currentlyEnabledLEDs & (255 - LEDToDisable));
}

void CubeSat_CSEE::EnableLED(uint8_t LEDToEnable, int arrayAddressToMask)
{
	uint8_t currentlyEnabledLEDs = EEPROM.readByte(arrayAddressToMask);

	ConfigureToWriteToRegister(1);
	EEPROM.updateByte(arrayAddressToMask, currentlyEnabledLEDs | LEDToEnable);
}

uint8_t CubeSat_CSEE::GetByteMask(int LEDToMask)
{
	return 1 << (((LEDToMask - 1) % 8));
}*/

uint16_t CubeSat_CSEE::GetDataSize()
{
	return EEPROM.readInt(DATA_SIZE_ADDRESS);
}

/*uint16_t CubeSat_CSEE::GetMaskAddress(int LEDToMask)
{
	int arrayToMask = (LEDToMask - 1) / 8 + 1;

	int arrayAddressToMask;

	if (arrayToMask == 1)
	{
		arrayAddressToMask = LED_ENABLED_ARRAY1_ADDRESS;
	}
	else if (arrayToMask == 2)
	{
		arrayAddressToMask = LED_ENABLED_ARRAY2_ADDRESS;
	}
	else
	{
		arrayAddressToMask = LED_ENABLED_ARRAY3_ADDRESS;
	}

	return arrayAddressToMask;
}*/

uint16_t CubeSat_CSEE::GetSizeOfIndividualLEDData()
{
	return EEPROM.readByte(VOLTAGE_RESOLUTION_ADDRESS) * (SIZEOF_CURRENT + SIZEOF_VOLTAGE) + SIZEOF_LED_MARKER + SIZEOF_TIME_STAMP;
}

bool CubeSat_CSEE::IsEEPROMFull()
{
	return (GetSizeOfIndividualLEDData() + eepromAddressCounter) > (E2END - ALLOCATED_REGISTERS);
}

void CubeSat_CSEE::IncrementCurrentLED(uint8_t currentLEDInput)
{
	currentLEDInput++;

	if(currentLEDInput > 24)
	{
		currentLEDInput = 1;
	}
	ConfigureToWriteToRegister(1);
	EEPROM.updateByte(CURRENT_LED_ADDRESS, currentLEDInput);
}

void CubeSat_CSEE::UpdateDataSize()
{
	ConfigureToWriteToRegister(2);
	EEPROM.updateInt(DATA_SIZE_ADDRESS, eepromAddressCounter);
}

void CubeSat_CSEE::VoltageRamp()
{
	int current_mA = 0;
	int voltage = 0;

	for (int i = 0; i < 4096; i = i + 4096 / (GetVoltageResolution() - 1))
	{
		dacTransfer(DAC_PIN, i);
		delay(5);
		float voltage = (analogRead(VOLTAGE_READ_PIN) * 5. / 1023.);
		int voltageConversion = (int)(voltage * 100);

		ConfigureToWriteData(2);
		EEPROM.updateInt(eepromAddressCounter, voltageConversion);
		IncrementCounter(2);

		float current = ina219.getCurrent_mA();
		int currentConversion = (int) (current * 100);

		ConfigureToWriteData(2);
		EEPROM.updateInt(eepromAddressCounter, currentConversion);
		IncrementCounter(2);
	}

	dacTransfer(DAC_PIN, 0);
}

void CubeSat_CSEE::VoltageSet()
{
	int current_mA = 0;
	float voltage = 0;
	int i = 4096;
	
		dacTransfer(DAC_PIN, i);
		delay(5);
		 voltage = (analogRead(VOLTAGE_READ_PIN) * 5. / 1023.);
		int voltageConversion = (int)(voltage * 100);

		ConfigureToWriteData(2);
		EEPROM.updateInt(eepromAddressCounter, voltageConversion);
		IncrementCounter(2);

		float current = ina219.getCurrent_mA();
		int currentConversion = (int) (current * 100);

		ConfigureToWriteData(2);
		EEPROM.updateInt(eepromAddressCounter, currentConversion);
		IncrementCounter(2);
	

	//dacTransfer(DAC_PIN, 0);
}

void CubeSat_CSEE::AquireEL(MCDC04 tmp)
{
	uint16_t TrD , X, Z, Y;

	MCDC04_SetConfig();
	tmp.GetData(&X, &Z, &Y);

	ConfigureToWriteData(2);
	EEPROM.updateInt(eepromAddressCounter, X);
	IncrementCounter(2);

	ConfigureToWriteData(2);
	EEPROM.updateInt(eepromAddressCounter, Z);
	IncrementCounter(2);
	
	ConfigureToWriteData(2);
	EEPROM.updateInt(eepromAddressCounter, Y);
	IncrementCounter(2);

}

void CubeSat_CSEE::IncrementCounter(int bytesToIncrement)
{
	for(int i = 0; i < bytesToIncrement; i++)
	{
		eepromAddressCounter++;
	}
}

void CubeSat_CSEE::DecrementCounter(int bytesToDecrement)
{
	for(int i = 0; i < bytesToDecrement; i++)
	{
		eepromAddressCounter--;
	}
}

void CubeSat_CSEE::WriteLEDInfo()
{
	ConfigureToWriteData(3);

	EEPROM.updateByte(eepromAddressCounter, GetCurrentLED());
	IncrementCounter(1);

	//THIS IS FILLER.  FIGURE OUT HOW TO GET THE TIMESTAMP WHILE ON THE SATELLITE
	//EEPROM.updateInt(eepromAddressCounter, GetTimeStamp());
	//IncrementCounter(2);
}

uint16_t CubeSat_CSEE::GetTimeStamp()
{
	//THIS IS FILLER. FIGURE OUT HOW TO GET THE TIMESTAMP WHILE ON THE SATELLITE
	return 0;
}

void CubeSat_CSEE::MCDC04_SetConfig()
{
  MCDC04_1.SetState(0x02); //Configuration Mode Set
  MCDC04_1.SetCREGL(0x88); //Anodes Set and Input, Reference Current =  20 nA, Integration Time = 256 ms
  MCDC04_1.SetCREGH(0x08); 
  MCDC04_1.SetOPT(0x00);
  Serial.println("MCDC04 #1 Configuration Set");

  MCDC04_2.SetState(0x02); //Configuration Mode Set
  MCDC04_2.SetCREGL(0x88); //Anodes Set and Input, Reference Current =  20 nA, Integration Time = 256 ms
  MCDC04_2.SetCREGH(0x08); 
  MCDC04_2.SetOPT(0x00);
  Serial.println("MCDC04 #2 Configuration Set");

  MCDC04_3.SetState(0x02); //Configuration Mode Set
  MCDC04_3.SetCREGL(0x88); //Anodes Set and Input, Reference Current =  20 nA, Integration Time = 256 ms
  MCDC04_3.SetCREGH(0x08); 
  MCDC04_3.SetOPT(0x00);
  Serial.println("MCDC04 #3 Configuration Set");
}

void CubeSat_CSEE::TestEEPROM() {
	int i;
	ConfigureToWriteData(2048);
	for (i = 0; i < 2048; i++) {
		if (i % 2 == 0)
			EEPROM.updateByte(i, 0xAA);
		else
			EEPROM.updateByte(i, 0x55);

	}
}

void CubeSat_CSEE::TestRAM() {
	int i;
	for (i = 0; i < 2048; i++) {
		if (i % 2 == 0)
			savedData[i] = 0xAA;
		else
			savedData[i] = 0x55;

	}
}

void CubeSat_CSEE::RetrieveSavedData(uint8_t* dataArray, uint16_t packetID)
{
	for(int i = packetID * 32; i < ((packetID * 32) + 32); i++)
	{
		dataArray[i - (packetID * 32)] = EEPROM.readByte(i);
	}
}
